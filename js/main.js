document.documentElement.className=document.documentElement.className.replace('jsDisabled',' jsEnabled jsLoading');

(function(a){a.expr[":"].onScreen=function(b){var c=a(window),d=c.scrollTop(),e=c.height(),f=d+e,g=a(b),h=g.offset().top,i=g.height(),j=h+i;return h>=d&&h<f||j>d&&j<=f||i>e&&h<=d&&j>=f}})(jQuery);



$(function (){ //<==READY

		//Большие слайдеры
	  $('.frame-forwho .illustrationBlock,.frame-earn .illustrationBlock').each(function(){
    var owner = this;
    var html = [];
    //$('.slideList .item',this).each(function(){
     // html.push("<a href='#' class='item'><span></span></a>");
    //})

    $(this).append($("<div class='navBlock navBlock-steps'><div class='blockWrapper'><a href='#' class='step step-prev'><i class='material-icons'>arrow_back</i>Назад</a><a href='#' class='step step-next'>Вперел<i class='material-icons'>arrow_forward</i></a></div></div><div class='navBlock navBlock-items'><div class='blockWrapper'>"+html.join('')+'</div></div>'));

    this.__switch = function(idx){
      if (typeof(idx)=='undefined')
      {
        this.__current++;
      }
      else
      {
        if (idx=='prev'||idx=='next')
        {
          this.__current += idx=='prev'?-1:1;
        }
        else
        {
          this.__current=idx;
        }
      }

      if (this.__current <0)
      {
        this.__current = $('.slideList .item',owner).length;
      }
      if (this.__current >= $('.slideList .item',owner).length)
      {
        this.__current=0;
      }

      $('.slideList .item',owner).removeClass('item-active').eq(this.__current).addClass('item-active');
      $('.navBlock-items .item',owner).removeClass('item-active').eq(this.__current).addClass('item-active');
    }
    this.__switch(0);

    $(this).mouseleave(function(){
      this.__interval = window.setInterval(function(){
        owner.__switch();
      },($(owner).data('item-interval') || 5)*1000);
    }).mouseenter(function(){
      if (this.__interval) {window.clearInterval(this.__interval);}
    }).mousemove(function(){
      if (this.__interval) {window.clearInterval(this.__interval);}
    })

    $('.navBlock-steps',this).on('click','.step',function(e){
      e.preventDefault();
      owner.__switch($(this).hasClass('step-prev') ? 'prev' : 'next');
      return false;
    });

    $('.navBlock-items',this).on('click','.item',function(e){
      e.preventDefault();
      owner.__switch($(this).index());
      return false;
    });
  });
    
  $(".frame-faq .faqList").on("click",".item .questionBlock", function () {
    $(this).parents(".item").toggleClass("item-view");
  });
  $(".tablePrice .advantageBlock").on("click",".title", function () {
    $(this).parent().toggleClass("open");
  });


  /*Мелкие слайдеры*/

$('.frame-reviews .slideList, .frame-players .slideList').each(function(){
  $(this).wrap($("<div class='catalogScrollBlock'><div class='scrollWrapper'></div></div>"));
  var owner=this.parentNode.parentNode;
  $(owner).prepend($("<div class='scrollNavBlock'><a href='#' class='step step-prev'><i class='material-icons'>arrow_back</i>Назад</a><a href='#' class='step step-next'><i class='material-icons'>arrow_forward</i>Вперед</a></div><div class='navBlock navBlock-slides'></div>"));
  var $list = $(this);
  var $outer = $list.parent();
  
  $('.item:first-child',$list).addClass('item-active');
  
  owner.__repos = function(auto)
    {
      if (!this.__offset) {this.__offset=0;}
  
  
     var listWidth = $('.item:last-child',$list);
     listWidth = listWidth.outerWidth(true)+listWidth.position().left;
  
     var steps = [];
     steps.length = Math.ceil(listWidth/$outer.outerWidth());
  
     $('.navBlock-slides',owner).html('<a class="slide" href="#">'+steps.join('</a> <a class="slide" href="#">')+'</a>');
  
     if (auto==true )
     {
       var diff = owner.__offset/$outer.width();
       var diffCount = Math.floor(diff);
  
       owner.__offset = $outer.width()*Math.round(diff-diffCount);
    }
  
     $('.scrollNavBlock .step',owner).removeClass('step-inactive');
     $('.navBlock-slides .slide',owner).removeClass('slide-active');
  
     if ($outer.width()<listWidth)
     {
       $(owner).addClass('isOverflown');
  
      $('.scrollNavBlock .step',owner).removeClass('step-inactive');
      if (owner.__offset<=0)
      {
       owner.__offset = 0;
       $('.scrollNavBlock .step-prev',owner).addClass('step-inactive');
       }
      if (owner.__offset+$outer.width()>=listWidth)
      {
       owner.__offset = listWidth - $outer.width() ;
       $('.scrollNavBlock .step-next',owner).addClass('step-inactive');
      }
    }
     else
     {
      $(owner).removeClass('isOverflown');
      this.__offset = 0;
      $('.scrollNavBlock .step',owner).addClass('step-inactive');
     }
     this.__offset=Math.round(this.__offset);
     var idx = Math.floor(this.__offset/$outer.width());
  
     $('.navBlock-slides .slide:eq('+idx+')',owner).addClass('slide-active');
  
     $list.css('left',-this.__offset);
    }
  owner.__repos(true);
  
  $(owner).on('click','.navBlock-slides .slide',function(){
    var idx = $(this).index();
    owner.__offset = idx * $outer.width();
    owner.__repos(true);
    return false;
  
  
  })
  
  $(owner).on('click','.scrollNavBlock .step',function(){
  var direction = $(this).hasClass('step-next') ? 1 : -1;
  //console.log(direction);
  if ($(owner).hasClass('imageGalleryBlock-compact'))
  {
    var idx = $('.item-active',$list).index() + direction;
    if (idx<0)
    {
      idx=$('.item',$list).length-1;
    }
    if (idx>$('.item',$list).length-1)
    {
      idx=0;
    }
    $('.item',$list).removeClass('item-active').eq(idx).find('>a').click();
  }
  else
  {
    owner.__offset = owner.__offset + direction * $outer.width();
  }
  owner.__repos();
  return false;
  })
  
  $('.item:first-child a',$list).click();
  
  });
  

  //Tabs
  $(".tabBlock").on("click",".switchTabList .item", function () {
    if ($(this).hasClass("active")) {
      return false;
    }
    else {
    var owner = $(this).parents(".tabBlock");
    var tabBlock = $(".illustrationWrapper",owner);
    var tabType =  $(this).data("type-tab");

    $(this).parents(".switchTabList").children(".item").removeClass("active");
    $(".illustrationBlock",tabBlock).removeClass("active");

    $(this).addClass("active");
    $(".illustrationBlock[data-type-tab="+tabType+"]",tabBlock).addClass("active");
    }
  })

	$(window).scroll(function(){
		var sT = $(window).scrollTop();
		var nH = $('header .navigationBlock').height();
		var wH = $(window).height();


		$('.frame-aboutgame .illustrationBlock .illustrationList, .catFly').each(function(){
      if ($(this).is(':onScreen'))
			{
				$(this).addClass('isInView');
			}
    });



$('input,textarea').each(function(){
  var val = $.trim($(this).val());
  this.value=val;
  $(this).parentsUntil('.fieldList').filter('.field').last()[val?'addClass':'removeClass']('field-state-filled');
})

$('body').on('focus','.title+.wrapper>input,.title+.wrapper>textarea',function(){
  $(this).parentsUntil('.fieldList').filter('.field').last().addClass('field-state-focused').find('.errorMessage').remove();
});
$('body').on('blur','.title+.wrapper>input,.title+.wrapper>textarea',function(){
  $(this).parentsUntil('.fieldList').filter('.field').last().removeClass('field-state-focused');
});
$('body').on('change','.title+.wrapper>input,.title+.wrapper>textarea',function(){
  var val = $.trim($(this).val());
  this.value=val;
  $(this).parentsUntil('.fieldList').filter('.field').last()[val?'addClass':'removeClass']('field-state-filled');
});

$('.frame-order .requestForm').submit(function(e){
  $(this).addClass('form-state-processing');
  
  var formData = $(this).serialize();
  
  $.post(this.action, formData, function(data){
    $(this).removeClass('form-state-processing');
    var html = $(data).find('.frame-order .requestForm .resultBlock')
    var isError = html.hasClass('resultBlock-error');
    html = html.html();
    $.magnificPopup.open($.extend(true,{},__opts.popup.defaults,{items: {src:$("<div class='popupBlock popupBlock-"+(isError?'error':'success')+"'><div class='blockWrapper'>"+html+"</div></div>")},type:'inline'}));
  }, 'html').fail(function(){
    $.magnificPopup.open($.extend(true,{},__opts.popup.defaults,{items: {src:$("<div class='popupBlock popupBlock-error'><div class='blockWrapper'>Возникла ошибка при отправке данных</div></div>")},type:'inline'}));
  }).always(function() {
    $(this).removeClass('form-state-processing');
  });
  
  return false;
})

});

});
$('html').removeClass('jsLoading').addClass('jsLoaded');